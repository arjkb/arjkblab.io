---
title: "Vagrant Hiccups"
date: 2021-01-31T15:50:06+05:30
draft: false
---
*(This is not a criticism against Vagrant; it has been a useful tool which I would continue to use it and encourage others to use as well.)*

This post is a note of the minor hiccups I faced while setting up Vagrant, and the steps I took to solve them, so that I can refer this later if required. This will   be a living-document which I'm hoping I would have to update only rarely.

Though I already installed Vagrant months ago (for Homestead) I figured I might as well update it. This decision turned out to be important later.

The update instructions turned were a little vague -- it says [here](https://www.vagrantup.com/docs/installation/upgrading) to "Install it over the existing package", which in hindsight makes sense but it wasn't obvious to me at first. Perhaps people who use Vagrant know what they're doing. Anwyay, I downloaded the latest binary from their website and put it in the exact spot where the old binary was. That way, I wouldn't have to change `$PATH`. And it worked!

## Hiccup #1
I faced some errors while following their quickstart:

<!-- {{< highlight zsh>}} -->
<!-- {{< / highlight >}} -->

```
$ vagrant up
No usable default provider could be found for your system.

Vagrant relies on interactions with 3rd party systems, known as
"providers", to provide Vagrant with resources to run development
environments. Examples are VirtualBox, VMware, Hyper-V.

The easiest solution to this message is to install VirtualBox, which
is available for free on all major platforms.

If you believe you already have a provider available, make sure it
is properly installed and configured. You can see more details about
why a particular provider isn't working by forcing usage with
`vagrant up --provider=PROVIDER`, which should give you a more specific
error message for that particular provider.
```
Let's try with the provider argument. 
```
$ vagrant up --provider=virtualbox
The provider 'virtualbox' that was requested to back the machine
'default' is reporting that it isn't usable on this system. The
reason is shown below:

VirtualBox is complaining that the kernel module is not loaded. Please
run `VBoxManage --version` or open the VirtualBox GUI to see the error
message which should contain instructions on how to fix this error.
```
That wasn't very useful. 
```
$ VBoxManage --version
WARNING: The vboxdrv kernel module is not loaded. Either there is no module
         available for the current kernel (5.8.0-7630-generic) or it failed to
         load. Please recompile the kernel module and install it by

           sudo /sbin/vboxconfig

         You will not be able to start VMs until this problem is fixed.
6.1.10r138449
```
Recompiling the kernel sounds like overkill, considering Homestead (which has Vagrant underneath) was working a few weeks ago. To confirm Homestead continued to work, I went over to the Homestead directory and did a `vagrant up`, and to my dismay it failed with error messages similar to the ones shown above!

Time to Google! After a couple of dead ends, somebody said "upgrade both Vagrant and VirtualBox". Opened up VirtualBox to discover that it was a few minor versions behind; updated to the latest, and voila, Vagrant stopped complaining!

## Hiccup #2

Now that the previous issue was fixed, I continued the official quick start:
```
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'hashicorp/bionic64' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'hashicorp/bionic64'
    default: URL: https://vagrantcloud.com/hashicorp/bionic64
==> default: Adding box 'hashicorp/bionic64' (v1.0.282) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/hashicorp/boxes/bionic64/versions/1.0.282/providers/virtualbox.box
Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
The executable 'bsdtar' Vagrant is trying to run was not
found in the PATH variable. This is an error. Please verify
this software is installed and on the path.
```

Uh oh; `bsdtar`? What's that? I'm on Linux! Why are you looking for BSD stuff? Googling told me that this barticular package has something to do with managing archives. And the error message is documented in their GitHub repo [here](https://github.com/hashicorp/vagrant/issues/8556). The recommended solution was to install `libarchive`.

Just for curiosity, I searched my package manager to see if it has bsdtar. It didn't, but it too recommended that I install `libarchive-tools`.
```
(base) ➜  ~ apt search bsdtar 
Sorting... Done
Full Text Search... Done
libarchive-tools/focal 3.4.0-2ubuntu1 amd64
  FreeBSD implementations of 'tar' and 'cpio' and other archive tools
```
Installing `libarchive-tools` sorted out that issue.

I'm glad the hiccups happened, because I learnt a little bit more and know how to avoid those in the future.