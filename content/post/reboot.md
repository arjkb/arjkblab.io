---
title: "Reboot"
date: 2020-09-08T21:42:21+05:30
draft: false
---

It's been unacceptably long since I last blogged. I won't even bother making up an explanation. 

However, I'm planning to get blogging again.

By now I have forgotten how Hugo worked. So I'm going to have to get up to speed on that. This post is actually just to see if the whole build pipeline for this hugo site still works. Honestly at this point I have no idea how that works.

Also, my theme -- which used to be on the Hugo themes showcase -- has apparently been removed because I stopped maintaining it. I intend to get this theme back in the showcase again. 