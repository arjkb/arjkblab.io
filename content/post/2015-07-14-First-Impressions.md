---
layout: post
comments: true
title: "First Drive: Blogging with Jekyll"
date: 2015-07-14
---
To be outright honest with you, setting up the whole thing took longer than I originally anticipated. But in the end, things seem to be more or less alright as of now. The official docs at Jekyll's isn't as helpful as I'd have liked, and I had to extensively look around the web to figure out how to do certain tasks.

Like I mentioned, the one serious grouse that I do have is that I do not have a custom domain. And just for this one simple reason alone, I debated whether I should switch back over to my old site on WordPress. However, upon further thoughts, I concluded that being on github.io domain in itself speaks to the world that this blog is more or less technically oriented.

And then, WordPress also offered me something convenient -- post scheduling. I had the ability to schedule a post to be published at any time I wanted to. Now, there are ways to do that in Jekyll, but then the whole process is somewhat complicated, especially when compared to what I'm used to with WordPress. Anyway, at the moment, I don't hava a dire need to schedule posts. Apart from that, I haven't yet fully got the hang of drafting posts. Perhaps I'm feeling that coz' I'm new to the system, but drafting posts is a clumsy affair indeed!

As of now I have no idea whether there's any built-in support for providing statistics on blog-traffic. Perhaps there is, but then I doubt it. I might probably have to resort to plugging in some statistics-providing mechanism, like OneStat which I had used a few years ago.

Speaking of which, I haven't yet set up redirecting my old blog on WordPress to over here. As it turns out, you do have to shell out some bucks to purchase the redirect upgrade, which I'll be doing eventually. At the moment, I've just made a sticky post over there with links to this webpage.

Also, markdown is something I'm not particularly familiar with. Sure I have used it before, but then I never did have the need to use it on a regular basis. Guess I'll have to learn how to work with that as well. Also, to further facilitate my work with markdown, I've installed the Atom editor on my system. Ever since I formatted my system over a month ago, I did not have any serious text-editing to do (vim was good enough for all the editing which I did have).

My first preference regarding editors would've been Brackets, but then, Brackets had it's own share of minor issues. The deal-breaker was that it's particularly weird to start it up from the command-line. (Don't know if they've fixed that issue by now).

Speaking of issues, Atom has it's own share of them as well. It's just been over an hour since I've started using Atom, and already the preview option has stopped working it seems. The program hangs and eventually crashes whenever I select the preview option. Hmm...I'll work on fixing that once I'm done with this post.