---
layout: post
title: First Impressions with Go
date: 2017-10-19
modified:
categories:
excerpt: Picked up Go a week ago, and this is what I think
tags: [go, golang, programming, socket, networks]
image:
feature:
date: 2017-10-19T16:20:56-06:00
hrefTargetBlank: true
---
The upcoming assignments for my networks class require fair amounts of socket programming. C is a double edged sword. Java is too verbose for my tastes. Python, despite all its beauty, is slow. And Haskell is not allowed for the networks assignments.

It was time to pick up a new language.

After contemplating on many alternatives, I finally settled on [Go (aka golang).](https://golang.org/) It is

* Simple to learn
* Statically typed
* Compiled
* Faster than most other languages
* Almost purpose-built for systems and networks programming
* Has excellent support for concurrency

Go is a winner for my purposes.


### Resources Used for Learning Go
In addition to numerous webpages, these are the two primary resources I used:

1. [An Introduction To Programming in Go](http://www.golang-book.com/books/intro) by Caleb Doxsey.
2. [The Go Programming Language](http://www.gopl.io/) by Brian W. Kernighan, and Alan A. A. Donovan (aka "K&D")

The first book was immensely useful to quickly get upto speed with the language. It is short, and would take only a day to read competely. However, the book is targeted at beginner programmers and lacks depth.

The second book above is the one to pick. The authors need no introduction for those in the know. Clear and crisp, K&D provides detailed explanations of langage features and its design decisions, and is a reference for idiomatic usage of the language. There is a fair amount of non-trivial examples -- a welcome feature as most other programming language books feature only trivial examples -- which when worked through gives you more insight. *K&D is a classic!*

### First Project Using Go: Dependency Setter for TaskWarrior
As a learning exercise, I created a simple tool using Go that can handle dependencies in taskwarrior.

[Taskwarrior](https://taskwarrior.org/) is an excellent command-line utility for managing your tasks and to-do lists. Capable of handling complicated workflows, it's the only to-do list that has worked really well for me. As this is not a post about taskwarrior, I recommed you check out [this excellent intro to taskwarrior](https://fedoramagazine.org/getting-started-taskwarrior/) to find out more about it.

Taskwarrior has this feature where you can specify dependencies between multiple tasks.

Let's say you have 4 tasks:

|   |   |
|---|---|
|21.|Download homework question|
|22.|Do homework|
|23.|Prepare report|
|24.|Submit report|

You wish to encode the fact that task 24 should be done only after 23, which inturn should be done only after 22, which inturn should be done only after 21. Taskwarrior allows you to set up this dependency follows:
```
$ task 24 modify depends:23
$ task 23 modify depends:22
$ task 22 modify depends:21
```

It can get cumbersome to set dependencies this way if you have a lot of tasks. And I'm not aware of any one-liner to set the dependencies.

And so, I wrote `tw_set_deps`, a simple Go script to set up the dependencies based on the command-line arguments passed into it. For instance, the following command does what the series of `task` commands do above:
```
$ ./tw_set_deps 21 22 23 24
```

It is not the most idiomatic code out there, and I could probably improve the code further after I learn more, but it was a good learning experience nevertheless. Check out the script [here.](https://github.com/arjkb/tw_set_deps)

Overall, I'm impressed with the Go programming language. Like most other creations at Google, Go is simple to pick up, and by design avoids many of the mistakes programmers typically make. It is statically typed, compiled, and runs faster than Python. Go also has an interesting take on concurrency.

#### Useful Links
* [Go official website](https://golang.org/)
* [Go at Google: Language Design in the Service of Software Engineering](https://talks.golang.org/2012/splash.article)
