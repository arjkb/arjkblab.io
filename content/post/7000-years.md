---
title: "7000 Years From Now"
date: 2021-01-21T23:24:20+05:30
draft: false
---

I don't remember how I got there, but while having a virtual conversation with friends in a Signal group, I wondered how life in the year 7534 would be like. Will there be cars? Will people be teleporting instead? Would America still exist? Would people have conquered other planets? Or would the entire earth as we know it be destroyed by then?

As I'm typing this, I realize 7534 is only a little over 5500 years from now (2021 + 5500 + change) instead of 7000, but I won't bother changing the title of this post or anything. And for some reason, 5500 doesn't seem as far off as 7000. That's probably because it isn't.

There were some casual questions that came up in the group:

* Will there be a 7534?

    Why wouldn't there be? There's actually a couple of ways I could tackle this:
    1. The Earth keeps rotating about its axis and revolves around the Sun. It has been doing so for a coupe of billion years at least. I'm not aware of any reason why that would stop in the coming few thousand years.
    2. It doesn't even matter whether or not the Earth (or the Sun or whatever) exists. the clock should keep ticking, and we should eventually reach 7534. I never really understood the implications of Einstein's theories of general/special relativity (or whether that is even relevant to the topic at hand), but I understand time gets wacky when you're in space. Even so, time should still move forward, so eventually you'll reach the point where it would have been 7534?
    3. Let's try ~~elementary~~ rudimentary mathematical induction. 2020 existed. 2021 existed. 2022 would exist (why not?). And so on and so forth until eventually 7534 also exists. Of course this is a flawed argument as it ignores very real physical phenomena that would eventually occur. Because we know from available data that the Earth and the solar system and everything has an expiration date (hopefully way past 7534). Therefore, even if we have a 7534, we cannot be so sure about 234209489061534237846982565. *But then again we wind back to the previous point of the Earth not being necessary...*

* Humans would have settled to another planet by then, and have reset the year to start from 1.

    * The year number is just an abstraction, to begin with.
    * Due to lack of clarity on my part, my friend took the 7534 -- a random year -- a little too literally. My question was about what life would be like roughly ~~7000~~ 5500 years from now. It doesn't matter whether they (who?) reset the year number in between.
    * In this specific example, there's actually a clear answer that "5500 years from 2021, humans would have settled another planet".


Here are some speculations on what I think might happen by then:
1. Fossil fuels would probably not exist. Or there would be very little reliance on it.
1. Planes would go electric (because of the point 1), and will be supersonic.
1. 100% of world's energy consumption would be from renewable sources.
1. The concept of countries as we know it today wouldn't exist. There would be no need for visas or passports or anything, and people would be able to travel anywhere they want to. Just like how they do it [Schengen](https://en.wikipedia.org/wiki/Schengen_Area) countries, but on a global scale. By extension there would also not be any kind of nationalism or anything at that point.
1. Routine space travel would be a thing.
1. Memories of computers would be so huge it might take a few terabytes just to address all available locations in memory.
1. Somebody might prove P == NP, and a whole lot of crazy stuff might immediately happen.
1. Physical hard currency would not exist.
1. Teleportation would not exist.
1. Traditional universities would not exist.
1. AI everywhere
1. Something like [lazy evaluation](https://en.wikipedia.org/wiki/Lazy_evaluation), but on a human level -- nobody does anything unless they have to. But once they do, things would *reliably* work due to which you wouldn't need to do that thing in the first place.
1. Traditional repititive jobs would all be automated away. Non-repititive jobs that require some thought and decision making would mostly be done by AI. Humans would only do the most researchy jobs out there, and that too with heavy assistance from AI stuff.
1. Physical books (and perhaps even paper itself) would cease to exist. Everybody would read from some kind of digital equipment.
1. People would stop writing with a pen or similar tools. I already see signs of it in certain developed western countries where everybody types everything. Eventually our thoughts might automatically become words on some medium.
