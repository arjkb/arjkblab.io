---
layout: post
comments: true
title: Blogging The Hacker Way!
date: 2015-07-14
---
Just switched over to Jekyll, and I'm finding the experience a little bit different. If there's any one grouse I have at the moment, it's that I don't have a custom domain. I guess that's more or less okay for now.

More posts coming soon!