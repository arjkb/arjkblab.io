---
layout: post
title: Capture Packets Using T-Shark
date: 2017-11-27
categories:
excerpt: Snoop intenet traffic
tags: [tshark, wireshark]
image:
feature:
date: 2017-11-27T15:34:10-07:00
---

#### DISCLAIMER

*Snooping on real internet traffic is illegal in most countries. It is your responsibility to follow the appropriate rules of your locality. I cannot be held responsible if you do something wrong.*

*I did the following activities from a controlled virtual environment.*

---

Did you know that you can poke around network traffic using tshark? According to the [official tshark man page](https://www.wireshark.org/docs/man-pages/tshark.html):

*"TShark is a network protocol analyzer. It lets you capture packet data from a live network, or read packets from a previously saved capture file, either printing a decoded form of those packets to the standard output or writing the packets to a file. TShark's native capture file format is pcap format, which is also the format used by tcpdump and various other tools."*

Most linux distributions provide tshark through their official repositories. For instance, to install tshark on debian-based machines, run `sudo apt-get install tshark`.

Although tshark is a very versatile tool for viewing captured packet information, I recommend installing wireshark as well, which -- among other things -- provides a GUI to view captured packets.

### Simple Example

Let's dive right in and demonstrate a packet capture. I have two virtual machines (running linux) connected over an ethernet connection.

1. Machine 1 has address `192.168.33.1` on `eth1`
2. Machine 2 has address `192.168.33.2` on `eth1`

For simplicity, you can think about `eth1` as an ethernet port, which is a network interface. Your machine may have multiple network interfaces. You can run `ifconfig` from the command-line to see a list of interfaces your machine has.

In this demo, I'll ping machine 2 from machine 1. This is guaranteed to make packets go back and forth between the two machines (because that's [how ping works](https://www.unix.com/man-page/linux/8/ping/)). At the same time, I'll capture traffic on `eth1` interface of machine 2.

Overall, these are the steps I need to do:

1. [Set up the pcap file.](#1-setup-pcap-file)
2. [Start t-shark and set it to capture packets.](#2-start-t-shark)
3. [From any one machine, ping the other machine.](#3-ping)
4. [Examine the captured pcap.](#4-examine-the-captured-pcap)

#### 1. Setup PCAP file
Create an empty PCAP file and give it adequate permissions. Here's how you do it:

```
$ touch foo.pcap
$ chmod 666 foo.pcap
$ ls -l
total 4
-rw-rw-rw- 1 user user 1504 Nov 27 16:44 foo.pcap
```

These steps must be run from the machine you wish to run tshark on (machine 2 in our example).

The first two lines above create a new file `foo.pcap` and give it the necessary and sufficient permissions. The third line `ls -l` is a sanity check to ensure the permissions are as we expect; ensure it is `-rw-rw-rw-`.

#### 2. Start t-shark
As mentioned earlier, we will capture traffic on the `eth1` interface of machine 2. To do so, from the terminal of machine 2, run:

```
$ sudo tshark -w foo.pcap -i eth1
```
|||
|---|---|
|`-w`|specifies the file to write raw packet data to|
|`-i`|specifies the interface to listen on|

From this point on, tshark will capture all traffic that goes through `eth1` interface of this machine.

#### 3. Ping
From your other machine, ping the machine where tshark is running.

As I mentioned before, the address of machine 2 is `192.168.33.2`.

```
$ ping -c 4 192.168.33.2
PING 192.168.33.2 (192.168.33.2) 56(84) bytes of data.
64 bytes from 192.168.33.2: icmp_seq=1 ttl=64 time=0.335 ms
64 bytes from 192.168.33.2: icmp_seq=2 ttl=64 time=0.397 ms
64 bytes from 192.168.33.2: icmp_seq=3 ttl=64 time=0.206 ms
64 bytes from 192.168.33.2: icmp_seq=4 ttl=64 time=0.248 ms

--- 192.168.33.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 2999ms
rtt min/avg/max/mdev = 0.206/0.296/0.397/0.076 ms
```

|`-c`|specifies the number of ECHO_REQUEST packets to send|

#### 4. Examine the captured pcap

Go back to machine 2 where the packet capture is happening, and press `Ctrl + C` to kill the capture process.

The simplest way to examine the pcap would be to double-click-open it in wireshark.

You can also examine the packet using tshark as follows:
```
$ tshark -r foo.pcap
1 0.000000000 192.168.33.1 → 192.168.33.2 ICMP 98 Echo (ping) request  id=0x433b, seq=1/256, ttl=64
2 0.000029979 192.168.33.2 → 192.168.33.1 ICMP 98 Echo (ping) reply    id=0x433b, seq=1/256, ttl=64 (request in 1)
3 0.999566511 192.168.33.1 → 192.168.33.2 ICMP 98 Echo (ping) request  id=0x433b, seq=2/512, ttl=64
4 0.999589158 192.168.33.2 → 192.168.33.1 ICMP 98 Echo (ping) reply    id=0x433b, seq=2/512, ttl=64 (request in 3)
5 1.999376489 192.168.33.1 → 192.168.33.2 ICMP 98 Echo (ping) request  id=0x433b, seq=3/768, ttl=64
6 1.999403090 192.168.33.2 → 192.168.33.1 ICMP 98 Echo (ping) reply    id=0x433b, seq=3/768, ttl=64 (request in 5)
7 2.999407347 192.168.33.1 → 192.168.33.2 ICMP 98 Echo (ping) request  id=0x433b, seq=4/1024, ttl=64
8 2.999431033 192.168.33.2 → 192.168.33.1 ICMP 98 Echo (ping) reply    id=0x433b, seq=4/1024, ttl=64 (request in 7)
9 5.008882964 f2:b5:79:20:12:c9 → 72:4f:b2:65:2a:2f ARP 42 Who has 192.168.33.1? Tell 192.168.33.2
10 5.009241891 72:4f:b2:65:2a:2f → f2:b5:79:20:12:c9 ARP 42 192.168.33.1 is at 72:4f:b2:65:2a:2f
```

Things to notice:


1. Each line indicates (among other things) the source and destination ip addresses.
2. Each line indicates that the packets were an ICMP echo request or reply.
3. There are four (same as the `-c` argument of `ping`) ICMP echo requests from `192.168.33.1` to `192.168.33.2`, and four ICMP echo replies going the other way.

### What Next?

We have barely scratched the surface here.

tshark is a very versatile tool. You can extract a lot of information such as the source and destination ip addresses, hardware addresses, sequence and acknowledgement numbers, and even the textual content of websites (if they use an unencrypted connection such as `http`). *This is why passwords must never be sent over unencrypted connections.*

Check out `man tshark` for a full list of options tshark supports.

The [wireshark wiki page](https://wiki.wireshark.org/SampleCaptures) contains a list of sample packet captures you can explore with wireshark or tshark. For instance, examine  [`http.cap`](https://wiki.wireshark.org/SampleCaptures#HyperText_Transport_Protocol_.28HTTP.29) and see if there is a `SYN`, `SYN-ACK`, and `ACK` as per the specifications.

In addition to wireshark and tshark, there are plenty of other libraries you could use to analyze the traffic. Python's [dpkt](https://dpkt.readthedocs.io/en/latest/) is a good example.
