---
layout: post
title: Keyboard Shortcuts To Toggle WiFi
date: 2016-03-06
excerpt: <p> Tired of moving around the mouse for mundane tasks like turning ON/OFF your WiFi? Well, it's time to create a keyboard shortcut! </p>
---
Being developers, nearly all of us prefer minimal use of the mouse (or touchpad).

Now I don't know about you, but there are cases when I don't frequently have to browse the internet, and during those times I prefer to turn off my WiFi. *At the very least, it would save some power.*

But then, there are some cases when I just have to momentarily turn on my WiFi, only to turn it back off after a brief while. And, I kind of figured it's somewhat annoying to drag around the mouse to the corner of the screen to turn the WiFi on. To me, using a mouse for somewhat mundane tasks like turn ON/OFF wifi seems kind of lame.

And so, I decided to come up with a shortcut. The next second, I was googling on the best way to do this. Yes, people had talked about this before, and some of them had proposed various solutions, almost always requiring the installation of a third-party software which I wasn't so keen on doing for an apparently silly task.

And so, I came up with my own solution that should work fine for all linux machines. (At least, I'm pretty sure it would work fine on Fedora, Ubuntu, and distros closely related to them).

The basic idea is to set the keyboard shortcut for shell commands that would turn ON/OFF the wifi.

First up, open up your terminal and try out the following commands to see if the WiFi turns ON/OFF.

```
$ rkfill block wifi
$ rkfill unblock wifi
```

The first one turns off the WiFi, while the second one turns it back on. If things work as expected, you can now proceed to add a custom keyboard shortcut.

The straightforward graphical way to do this on Fedora 23 is:

1. Open `Settings`
2. Click on `Keyboard`
3. Click on `Shortcuts` tab

Somewhere towards the bottom of that window, you should see a "+" sign to add custom shortcuts.

A small window should pop up, asking you to provide a name for the shortcut, as well as the command to execute (you will provide the actual keyboard combination in the next step).

![]({{ site.url }}/assets/images/wifi_on_shortcut.png)

Type in a name that you prefer; it doesn't have too much significance here. I wrote in "WiFi ON".

In the command field, type in `rfkill unblock wifi`

Click on `Add`.

You should now be seeing your shortcuts (along with a list of other shortcuts that you may be having). Click on the shortcut which you just created. The text right beside it would say something like `New accelerator...` .

At this moment, press the keyboard combination that you wish to set as the shortcut.

And you're done!

*(Depending on your exact distro, there might be minor changes to the procedure. I leave it to you to figure that out).*

Note that you would need to repeat the process once again for adding the shortcut to turn off WiFi (ie., you've to do the whole process twice, one for turning on the WiFi, and one for turning off the WiFi).

Personally, I set `Ctrl + Alt + W` for turning on WiFi, and `Ctrl + Shift + W` for turning off wifi.

**Ensure that the shortcuts you set are not already in use by other programs!** You would usually be prompted when the shortcut you set clashes with some other shortcut already in use by the systen functionalities.

However, keep in mind that you'll only be alerted for shortcuts of other system functionalities, and not shortcuts used by other applications. For instance, I had initially set `Ctrl + W` for turning on WiFi, only to discover in a clumsy manner that the same combination was used for some other purpose by the Firefox browser.

Lastly, once you're done setting up the shortcuts, try it out and ensure that things work as expected!

*Happy shortcutting!*