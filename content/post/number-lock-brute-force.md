---
title: "Number Lock Brute Force"
date: 2021-03-14T11:00:02+05:30
draft: false
---
While cleaning his wardrobe a family-member discovered his old bag with a built-in number lock. He forgot the number and was contemplating detroying the lock to open it.

The lock had only two 10-digit combination; 100 possible numbers -- brute force is feasible!

The lock opened on `55`.

This lead me to contemplate the design of the bag and the number lock. Why would anyone design a number lock with just two digits? Anybody who steals the bag can find the correct permutation in less than 3 minutes -- blatant illusion of safety.

Things with numeric locks are fairly common around me. Apart from the usual trolley-bags, even my dorm-room in college had a numeric lock; not having to carry around a key is appealing.

How many digits is needed before brute-forcing becomes infeasible? 3 digits? 5? My intuition told me 7 or 8 digits should be rather hard. I decided to systematically find out.

It should become infeasible if the time to brute-force all possibilities becomes sufficiently large. Exactly how large is sufficiently large is up for debate, but I doubt if any adversary would spend more than a couple of hours trying all permutations (unless there's something extremely valuable inside).

Therefore, the time to try all permutations would be a function of the time to try one permutation and the number of possible permutations, which inturn is a function of the number of digits. For simplicity I'm assuming the lock has base-10 numbers for each possible digit. After some thought, I came up with the following formula:
```
(10^num_of_digits) x num_of_seconds_to_test_a_permutation
```

It's exponential (`O(10^n)`), and therefore grows rapidly as the number of digits increase.
The initial 10 is for the 10 possibilities per digit.

Assuming it takes 1.5 seconds to test a combination (including the time to set a number), here's an approximation of the time it takes to try all permuations for a given number of digits:


|Digit count   |&nbsp;&nbsp;Total seconds taken|&nbsp;&nbsp;Human-scale time|
|-----------------------:|------------------:|---------------------:|
|2| 150.0                       |&nbsp;&nbsp; < 3 minutes |
|3| 1500.0                      |&nbsp;&nbsp; < 30 minutes|
|4| 15000.0                     |&nbsp;&nbsp; 4+ hours |
|5| 150000.0                    |&nbsp;&nbsp; 1+ day|
|6| 1500000.0                   |&nbsp;&nbsp; 17+ days|
|7| 15000000.0                  |&nbsp;&nbsp; 5+ months|
|8| 150000000.0                 |&nbsp;&nbsp; 4+ years|
|9| 1500000000.0                |&nbsp;&nbsp; 48+ years|
|10| 15000000000.0              |&nbsp;&nbsp; 482+ years|


For 2 digits it took less than 3 minutes, which is consistent with our actual experience above. By the time it's 5 digits, it takes more than a day (and therfore infeasible); adding an additional digit brings the total time to 17+ days.

In practice I haven't seen any number-locks with more than 4 digits though.