---
title: "Getting a US student visa"
date: 2016-12-01T15:41:51+05:30
draft: false
---

Just wanted to write down the process of obtaining a US visa. None of this is legal advice. I'm just posting my own experience. Consult the official US govt websites for most accurate and uptodate information about visas.

People seem to have wrong ideas about the whole process and what happens at the consulate. And for student visa atleast, there is no need to hire a consultant or agent.

I did it twice so far -- once for an exchange visa (J1) and the second time for a student visa (F1). So I can only talk from such a perspective; the process is probably different for business/tourist visas, and definitely different for H1B, L1 etc. I applied from India; the process might be different for people in other countries.

### Preconditions

1. Ensure you have a valid passport.
2. Ensure -- atleast for Indians -- there's something in both the "Given Name" and "Surname" fields.
3. [Form I-20](https://studyinthestates.dhs.gov/students/prepare/students-and-the-form-i-20) from the US university you have been admitted to.

Plenty of people have nothing in the surname field; their first, middle (if any), and last names will all in the "Given Name" field of the passport (which is a totally fine thing in some parts of India). This can be a problem when you apply for a US visa. Even if you get a visa, it may (or may not) lead to problems once you're in USA and you need to provide your passport for some ID verification. My passport had a blank surname field -- before applying for a visa I obtained a brand new passport with my last name in the surname field. 

### Applying

You should already be admitted to the US university you want to attend, and must have the physical paperwork (in particular, the Form I-20) before you apply for the visa.

There is an official US govt website you can go to fill the application form. You can do it yourself. Filling out the application form can take a while because there are lots of things to fill up and questions to be answered. At the end you have to pick dates for the US consular interview. The consular interview is a two-step process that takes place on two different days (you can't do those two processes on the same day AFAIK):

* Day 1: they take your photograph and fingerprints.
* Day 2: interview with a US consular officer at the US consulate.

Pick two consecutive days for the above unless you live in a city that has a US Consulate or if you don't mind traveling back and forth. Depending on the time of the year, it may be easier or harder to get an appointment. Sometimes, the next available date might be several weeks ahead. 


### At the US Consulate
(In both times I went to get the visa, the process was the same).

The US Consulate at [Chennai](https://en.wikipedia.org/wiki/Chennai) was the one nearest to my home -- an hour away by flight.

The photo/fingerprint thing was not at the Consulate, but in a separate building. There was a long queue when I reached there, though the queue was moving at a fairly rapid pace. At multiple points, people -- they seemed to be Indians -- examined my paperwork. Finally, I was in a room with small photo-booths to take my picture and there was another device to record my fingerprints. I think all ten of my fingerprints were taken. The whole process -- from entry to exit -- must have taken an hour.

The Consulate General of the United States in Chennai is large facility and ranks high in terms of the number of visa applications that are processed worldwide. The entire structure and architecture looks distinct from the other buildings in the vicinity. 

On day 2, I was at the consulate half hour before my appointment. Trivia: from my hotel, all I had to say to the taxi driver was "America", and he promptly dropped me in front of the facility. There was a long queue outside on the street, which was moving at a fairly good pace. Immediately after you enter the gates, you reach a small building where officials do a quick check of your paperwork and then frisk you. You aren't supposed to bring in any electronics inside. My dad was outside holding my phone, but I think (not sure) there's some facility to hold your cellphones and stuff if you travel alone.

From the first building you walk an open courtyard to a bigger building nearby in the same compound. There were multiple old Toyota Land Cruisers adorning consulate plates in the courtyard.

Eventually the queue led me to two officers who verified my fingerprints. This is the first time in this whole process that I'm actually face-to-face with an American (everyone else -- the people who glanced through my paperwork, and the people who took my photo and fingerprints were all Indians). Next, I reached a bigger hall which again had a long winding queue.

The hall was large, well lit, and reminded me of a ticket-reservation room of a train station, only a lot cleaner. There were 7 or 8 counters with glass partitions and mic and speaker on both sides of the partition. Behind the counters stood the US consulate officers, who were talking to the visa applicants at their respective counters. Everybody standing in the queue could see what was going on. Some people had a fast processing time while for others it took slightly longer. A ballpark estimate is that it took 5 to 10 minutes to process an applicant, although this might vary wildly depending on the applicant's unique situation. Some people were infact getting their visas rejected (you could hear "no visa for you" on the mic). There were interpreters for Indian languages available for applicants who did not speak English.

Eventually I was at the front of the queue and was assigned a counter. At the counter the extremely friendly consular officer asked me a few quick questions about why I'm going to the US, what people in my immediate family did (all of them has white collar jobs), whether any of them were in the US (they weren't), and whether any of them had ever gone to USA before (they hadn't). All the while he was typing stuff onto his computer screen. He didn't really delve too much into how I was going to pay for my stay in USA, although I did have a scholarship and additional money and had proofs of the same.

At the end he said something like "Alright visa approved! Enjoy your trip!". That was it. The meeting might have taken a total of 3 minutes maybe. In fact, this seemed so trivial that for a moment I wondered what all the fuss was about.

### Getting the Visa

The consular officer had collected my passport. While originally applying for the visa (via that big online form) there was a field to choose how I'll pick up my passport once the visa was pasted onto it. There is an option to collect it directly from the consulate after a few days, which was infeasible for me because I live in a different city. I opted to have it delivered to a visa collection center near my home. (For some reason they don't send it directly to your address).

Pasting the visa onto my passport and shipping it to the visa collection center took 5 days.

### Conclusion

For student visa, the process is straightforward: apply online and set up an appointment, attend the interview, and wait for your passport (with visa pasted on it) to arrive back home. There is no need to consult an agent.