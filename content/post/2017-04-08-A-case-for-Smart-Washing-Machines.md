---
layout: post
title: A Case for Smart Washing Machines
date: 2017-04-08
excerpt: What if you could remotely observe your washing machine?
tags: washer laundry IOT raspberry-pi ideas
---
The residence hall I live in offers washing and drying facilities to wash and dry our clothes.

There is one dedicated room for the washers and dryers. In the rest of this article, I will be calling this room the "laundry-room". The washing and drying takes approximately 30 and 45 minutes respectively, depending on the mode you choose. Of course, once the washing is done, you have to manually move your clothes from the washer to the dryer.

All is not well however. Sometimes all the washers in the residence hall might be engaged simultaneously. This is not a remote possibility -- there are about 170 students in my residence hall and four washers (and an equal number of dryers). There is absolutely no way to know whether any of the washer is free unless you go to the laundry-room and look.

The problem is proportionately more annoying the further away your room is from the laundry-room. For instance, I live on the second floor, and the laundry-room is on the basement. I've lost count of the number of times I've gone to the laundry-room with my laundry-basket full of clothes, only to climb back up the stairs again with all of that baggage, feeling like an idiot. And then later, you'll have to go down to the basement again to see if the washers have freed up.

Going down to the laundry room a second time may not always solve the problem however. Often times people are not prompt in removing their clothes from the washers once the washing is done, although the washers have timers on them.

Some folks get around this situation by actually removing other people's clothes from the idle washers. Personally, I prefer not to touch other people's clothes (even if it's supposedly clean, having been just washed). And for that matter, I don't like other people touching my clothes either. Touching other people's wet clothes is just gross. Period.

**The bottom line is, you never know whether the washers are free unless you go and look.**

What I need is a way to know whether the washers and dryers are free without going all the way to the laundry room. How difficult can this be to implement?

At this point, there are two approaches I can think of:

1. Extract data from the washer's embedded computer.
2. Literally "look" inside the washer to see if there are clothes inside.

### Approach 1: Data Extraction
When it comes to washers, there are some things in our favor. Every washer has an embedded computer inside that keeps track of the current-state of the washer -- spinning, soaking, etc. And in any case, the washers in question already has a timer that's aware of exactly how long the wash is going to take depending on the mode you choose.

So one approach is to extract this information from the washer and publish it somewhere (to a website, mobile app etc).

For this to work, the microcontroller controlling the washer needs to be modified so that it passes relevant data to some nearby computer, which inturn updates a website, for instance.

However, extracting the information from the washer is complicated. Modifying the existing washers is something I'm not too keen on doing, especially since they're not open source and it can be tricky to figure out how its embedded microcomputer works in the first place. (This emphasizes the importance of making products open-source, so that people can modify the devices and make it suit their special needs).

### Approach 2: Image Processing
This approach does not involve modifying the embedded computer of the washer. Basically, we mount a camera on top of each of the washer, which inturn "looks" inside the tub to see if there are clothes in there. The image based solution might actually be cleaner; in approach 1, there is no guarantee that there are no clothes inside the washer even if it's not washing at the moment.

Image recognition technology is getting better each day. I mean, if there are technologies today that could detect traffic lights and cats, then technology to look inside a washer and determine whether there is a cloth inside could totally be created -- make a classifier or something and train it with thousands of images containing both pictures of washers with and without clothes in them.

Alternately, there are reliable cloud services to detect the objects in an image. It's only a matter of building a device to capture the image and send it to the cloud.

And it doesn't even have to be expensive either. Here is an approximate breakdown of the costs:

1. [Raspberry Pi Zero W](https://www.raspberrypi.org/products/pi-zero-w/) ($10 per unit).
2. Camera module (about $25 per unit).
3. Necessary connecting wires, memory-card etc (about $25 per set).
4. Image recognition cloud service charges (less than $2 per month. Decreases as the number of images increases).
5. Cost of programming and setting up the whole thing (assign it as a side-project to CS or electronics students in a university. It's a win-win).

I believe the cost can be recovered sufficiently quickly, considering the sheer numbers of students who tend to live on campus.

The technology exists to make the world a smarter and more connected place. It is upto us hobbyists and tinkerers to leverage the technology and make more useful stuff that benefits people.

Also, as more and more people start to play with IoT, it is ever more important for manufacturers of devices to embrace open-source technologies.
