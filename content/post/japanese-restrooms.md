---
title: "Japanese Restrooms"
date: 2023-03-29T11:52:46+05:30
draft: false
---

Toilets in Japan are a contraption in itself. I literally dropped my jaws when I noticed the controls.

![](/images/japan_mar_apr_2023/restroom_controls.jpg)

First up, the seat is heated! I've heard of heated and ventilated seats in cars but this is the first time I'm seeing a heated toilet-seats. I realized that heated toilet-seat is exactly what you need on a frigid morning!

Next is the wiping mechanism. At the press of a button a "wand" extends and sprays water at your, well, hole. In addition to finer adjustments to pinpoint the exact location where you want the water to be sprayed, you can also adjust the pressure of the water. For the ladies there's a button to spray water from the front. The whole thing just blew my mind.

The privacy button presumably plays other noises while you're doing your business. There's toilet paper as well. I didn't see a hose like you find in India though. 

Needless to say, the whole thing was spotlessly clean and dry (and warm!).

The picture above was taken from Narita airport, but these kinds of restrooms are everywhere.
