---
title: "UNM New Arrival Guide"
date: 2017-09-18T13:02:57+05:30
draft: false
---

**Disclaimer: This is UNOFFICIAL. I'm just documenting how things were like for me. It may have changed. I cannot be held responsible if anything goes wrong after following this post.**

(An unofficial guide for students from [Amrita Vishwa Vidyapeetham](https://www.amrita.edu/) planning to study at the [University of New Mexico](https://www.unm.edu/)).

If you've been admitted to UNM, congratulations!

I studied at UNM twice:
* as an undergrad exchange student during Spring 2016
* as a graduate student (MS) in Computer Science (CS) from Spring 2017 to the end of Fall 2018.

![Duck Pond](/images/unm-guide/duck_pond.jpg "Duck pond")
*The [Duck Pond](https://campushistory.unm.edu/essays/duck-pond) at UNM on a bright sunny day. It's much larger than it seems in the photo.*

---  

## Table of Contents

1. [Academics](#academics)
2. [Accomodation](#accomodation)
3. [Climate](#climate)
4. [Food](#food)
5. [Healthcare](#healthcare)
6. [Life in Albuquerque](#life-in-albuquerque)
7. [Punctuality](#punctuality)
7. [Visa](#visa)

### Academics

UNM has an [online course catalog](http://catalog.unm.edu/catalogs/2020-2021/) that displays the course number, name, description, number of credits, and pre-requisites. Check it out (Google "unm catalog" or something).

A course here typically has 3 credits. U.S. government rules stipulate the minimum number of credits international students must register for each semester. The last time I checked (please re-check!), it was:

* 12 credits for international undergrad exchange students.
* 9 credits for international grad students.
* 6 credits for international grad students who are a TA or an RA.

"Grad" is the same thing as "postgraduate" in India.

Be forewarned: those 9 credits require a lot more work than 30 credits per sem' in Amrita!

Once your paperwork is done and you're actually admitted to UNM, that's when you register for classes. You do it online through a mechanism similar to [AUMS](https://www.amrita.edu/aums). Seats are limited and fills quickly; do not delay the registration if you wish to get into your preferred courses. Certain category of students get to register earlier than others. For instance, grad students can register earlier than undergrads. Timings for different classes may clash, so you might have to pick and choose. Some courses are usually offered only in Spring, and others only in Fall. Ask around to get an idea of when the courses you wish to study are typically offered; or try to dig up past class schedules from the official websites and look for patterns.

Pro tip: I learnt the hard way that the 'R' in their course schedule stands for Thursday.

Nearly all departments publish a list of courses one must take and pass with a satisfactory grade before they can give you a degree. It is the responsibility of the student to actually register for those classes and make progress towards getting their degree. In addition to the required classes which you must enroll in at some point if you want to get your degree, you can take whatever other classes you want (subject to approvals in certain cases). For instance, if you're an CS major, but want to take classes on linguistics, economics, philosophy, or biology simply because you're interested in those, you can.

I know students who had to be here an extra semester solely because they needed one more class to meet their degree requirements. In the extremely unlikely situation that you need two more required classes on what was supposed to be your final semester, and they're both scheduled to be at the same time, then you're screwed.

For exchange students, UNM probably won't have your past academic records from your ungrad institution in their system and therefore your course registration would be rejected because you *apparently* haven't fulfilled the pre-requisites for the advanced classes you're trying to register for. Contact your international advisor, or the professor whose course you're trying to register for; explain that you're an incoming international student, and that you have the pre-requisite knowledge. They normally take your word for it, but be prepared to send them copies of your transcripts from your undergrad instituion if they ask for it.

It's a similar story with full time grad and undergrad students. I don't know about other departments, but you've to meet your CS department advisor personally before they allow you to register for classes.

Courses here are generally excellent, yet intense. With just 2 or 3 subjects on your plate -- typical for senior undergrads and graduate students -- you will have little free time. If a class is too difficult, you can drop that and register for another within the first couple of weeks after the start of classes. You'll have to check on that.

For most courses, there will be lectures twice or thrice a week, each lasting between 50 to 90 minutes. Labs (if applicable) will be in addition to the lectures.

Expect lots of assignments. These assignments alone will take up almost all your free time. The upside of blazing through these assignments is that you'll have an easier time on the actual tests. Sure you're going to have to study and really understand the material at hand, but once you do that then you will *usually* be able to score well.

Teachers are helpful, and would answer your questions candidly. Take advantage of their office hours to understand the material at hand, and also to get help with the homeworks. Homeworks are intellectual puzzles. Most teachers set them with the expectation that you will meet them during their office hours, so don't feel shy about walking into their office. I've often had very productive discussions with professors while trying to work through a homework.

![Zimmerman Library bookshelves](/images/unm-guide/zimmerman_library.jpg "Zimmerman library bookshelves")

The [libraries on campus](http://library.unm.edu/) are huge with [over 3 million books.](https://www.unm.edu/features/2013/unm-libraries-celebrates-three-million-books.html) However, your prescribed textbooks would likely not be in the library owing to reasons that are beyond the scope of this article.

Textbooks can be expensive. Price of a new textbook ranges between $20 and $200 (over &#8377;12,000 in 2016). Used textbooks are cheap and quality is on par with brand new books.

There are multiple ways to obtain textbooks:

* Purchase them through [UNM Bookstore](https://bookstore.unm.edu/).
* Purchase them online through Amazon.
* Rent your textbooks; you pay less than the actual price of the book, but you'll have to return the book at the end of the semester. (And no, you don't get your money back).

![Textbook price](/images/unm-guide/book_price.jpg "Textbook price")
*Sticker-price of one of my textbooks at the UNM Bookstore.*

Both Amazon and UNM Bookstore sells used books if available (in addition to brand-new books of course). UNM Bookstore is located right next to the campus. You'll find your textbooks there if your instructor ("teacher") requested it beforehand (they seem to have this behind-the-scene thing where the every instructor can declare the textbooks they would be using this semester, and the UNM Bookstore stocks them for students to buy). They also sell other items like stationery, UNM branded clothing and accessories, certain electronic gadgets etc. The bookstore sometimes offer buy-backs at the end of the semester; use this opportunity to recover part of your investment in textbooks.

##### Useful Links

* [UNM course catalog for 2017-2018](http://catalog.unm.edu/catalogs/2017-2018/)
* [Schedule of classes for Fall 2017](http://www.unm.edu/~rst/Schedule_of_Classes/Fall2017/SCHM/index.html)
* [UNM Bookstore](https://bookstore.unm.edu/)


### Accomodation
You can either live on-campus or off-campus. Living off-campus might be cheaper. Ms. Rakhi Rajagopal and Mr. Sasidharan Subramanyam -- both of whom were exchange students to UNM from Amrita Vishwa Vidyapeetham -- might be able to tell you more about off-campus accomodation. Contact ACIP for their email address or phone number.

I lived on-campus during my entire stay at UNM because of the safety, security, and convenience it offers.

To live on campus, you have to register on the [official housing website](https://housing.unm.edu) beforehand and make advance payment. "Hostels" in UNM are called "residence halls" (nobody says "hostel" in UNM). There are several halls to choose from, all with different number of roommates and amenities. The website includes details of the different residence halls, the types of rooms in each hall, their prices etc. I have lived in both [Hokona Hall](https://housing.unm.edu/residence-halls/hokona-hall.html) and [Santa Clara Hall](https://housing.unm.edu/residence-halls/santa-clara-hall.html), and they were both pleasant places to live, though I definitely prefer the former due to reasons I cannot quite put to words.

The rooms are neat and clean. At a minimum, you get:

* bed and cot
* desk with lockable drawer
* chair
* adequate shelves to store your things

You need to bring your own bedsheets, blankets, pillows etc.

![Hokona hall room](/images/unm-guide/hokona_115.jpg "Hokona Hall #115")
*This is room 115 of the Hokona Hall. There are drawers and open shelves on the other side of the room. The height of the bed can be adjusted; a lot of people like to "loft" their beds -- put it all the way to the top, and set up their desk underneath the bed.*

There were plenty of power sockets in each room. Power sockets in America look like [this](https://upload.wikimedia.org/wikipedia/commons/0/0d/B_plug.jpg), so your Indian power cords won't work; bring a power adapter. Some newer power sockets also have USB sockets built-in.

At the time I lived there, there were these central ~~air-conditioning~~ ventilation systems which pump hot air during the winter and cold air during the summer (ie., you can't get cold air during winter, and vice-versa). Note that your roommate's idea of how much is too hot or too cold may differ from yours.

Bathrooms aren't attached in all halls.

Laundry facility includes free common washers and dryers. Takes about 30 minutes to wash and 45 minutes to dry. You need to bring your own detergent -- preferably liquid instead of powder because that's what everyone uses, and there's a possibility that powdered detergents may solidify in the detergent tray and make it gross. I recommend getting a collapsible mesh laundry basket like the one shown [here](https://www.amazon.com/dp/B00HZ0ION8/) (not affiliated) to carry your clothes to the laundry room and back.

You get a free mailbox, but you have to explicitly sign up for it. Helps if you buy stuff from Amazon. Besides, you need an address before you can, say, start a bank account.

There's free WiFi everywhere on campus.

Note that **deciding to live on campus temporarily until you find an accommodation off-campus is a bad idea; you'll be subject to massive fines.** Also, if you do sign up for on-campus housing, but never showed up for whatever reason (like, maybe you decided not to live on campus) and you don't cancel your contract before the first day of class, you'll be fined -- as of Spring 2018 -- $2350; that's nearly 1.5 lakh rupees!

##### Useful Links
* [UNM Housing](https://housing.unm.edu)
* [UNM Housing Contract Cancellation](https://housing.unm.edu/living-on-campus/contract-cancellation.html)

### Climate
It is pretty cold during the winter. In January temperatures go well below 0&deg;C at night and upto about 10&deg;C during the day. Take a walk early in the morning, and you will see small lakes and ponds frozen solid, and ducks walking ON the frozen ice.

Heaters are in all rooms so you won't have a problem indoors. But you need multiple layers of clothing when stepping out. I recommend thermals, sweaters, thin jacket(s), and a thick jacket capable of withstanding rain and snow. Jackets made of cloth are ineffective against strong cold winds that will get through the fabric. However, they are convenient on days without snow, rain, or strong winds. Speaking of which, even if it's sunny and bright, temperatures could still be very cold.

![Snow outside Hokona Hall](/images/unm-guide/snow.jpg "Snow outside Hokona Hall")
*On the rare occasions that it snowed it did not persist for long; only a thin layer was on the ground which melted away quickly. There are rumous of heavy snow situations from the past, however.*

Temperatures in summer and early fall ranges from about 18&deg;C to 30&deg;C, and it will feel really hot during the daytime. And then there'll be those rare occasions when it'll be really windy, enough to make it impossible to walk without falling.

The air is dry. Moisturizing creams and lip-balms are must-haves. My lips were dry and broken within 24 hours of arriving here. On the flip side, once I landed back in India and walked out of the plane, I finally understood what "humidity" really meant. It felt like there was steam all over the place for a few days.


### Food

Food is NOT included with the accommodation. You have to pay extra to get food on campus.

There are several food outlets throughout the campus. Some offer just coffee or drinks, some offer sandwiches, and some offer cuisine from around the world.

The major dining hall inside the campus is the [La Posada](https://food.unm.edu/locations/la-posada/index.html) ("LaPo" in short). LaPo is open 24 hours a day, 7 days a week. You get different types of predominantly non-vegetarian cuisine.

Most people living on campus opt to eat from LaPo, which allows you to pay upfront for one of their meal-plans. The food is identical for all plans; the difference is in the number of times a day and the number of days a week you can go there. I chose an unlimited plan -- I can go there all 7 days of the week and as many times a day as I want. For this particular plan, I paid $1950 for the whole semester (back in 2017 and 2018; they may have revised the prices).

The meal plan is applicable only for LaPo. You have to pay out of your own pocket to eat at other food outlets within the campus, or anywhere else for that matter.

As I'm not crazy about food, I was able to manage just fine. However, if you are a foodie or a vegetarian, perhaps you might want to consider other options; instead of choosing a meal plan, you could cook your own food. Get a room with a kitchen; plan well and the money you save on the meal-plan would offset the higher rent of a room with kitchen.

![Rice in the market](/images/unm-guide/rice.jpg)

There are plenty of restaurants in the city, including a few Indian restaurants where the food is good in my opinion. A decent meal costs between $8 and $20. Restaurants here tend to expect a tip, like around 30% of the bill.

A coffee is between $2 and $5. You would inevitably drink a lot of those 😉.
(Try the Venti Iced White Chocolate Mocha with whipped cream from Starbucks @ [Zimmerman Library](https://library.unm.edu/about/libraries/zim.php). You'll love it!).

##### Useful Links
* [UNM Dine On Campus](https://dineoncampus.com/unm/)
* [UNM Food](https://food.unm.edu/)
* [UNM Meal Plans](https://food.unm.edu/meal-plans/unm-meal-plans.html)


### Healthcare
Healthcare in the United States is notorious for its exorbitant prices. A common joke is that Europeans in America can travel back to Europe on a first-class ticket, do their surgery there, and return back, and the total cost would still be cheaper than if you were to do the surgery stateside.

International students **must** have health insurance (it's a violation of UNM and U.S. policy otherwise). UNM's Global Education Office (GEO) website provides more information about obtaining health insurance; visit that page [here.](https://isss.unm.edu/students/health-care-and-insurance/health-insurance.html)

Medicines that are freely ("freely" as in, you can get it anywhere; not that they are free to purchase) available in India may not be freely available in the United States. You almost always require a prescription to purchase complicated medicines from the pharmacy. Keep that in mind if you're on medications. Having said that, medicines for common aliments such as headaches, cold, fever, or cramps can be bought over-the-counter.

UNM has a small clinic called [SHAC (Student Health & Counseling)](https://shac.unm.edu/), that would take care of your minor health issues. However, SHAC is a relatively small facility and is not designed to handle major issues (like, your arm got cut off).

##### Useful Links
* [Health resources](https://isss.unm.edu/students/health-care-and-insurance/health-resources.html)
* [Health insurance requirements for international students](https://isss.unm.edu/students/health-care-and-insurance/health-insurance.html)
* [SHAC website](https://shac.unm.edu/)
* [What to Do When You Get Sick?](https://isss.unm.edu/students/health-care-and-insurance/what-to-do-when-you-get-sick.html)


### Life in Albuquerque

Get rid of any preconceived notions you might have about "cities in America". Though Albuquerque is the largest city in the state of New Mexico, it's not mighty large. I would say it is smaller than Bangalore. Perhaps comparable to Kochi, Kerala.

The following are the common ways of travelling about in the city:
* Bus rides within the city were free for UNM students the last time I checked; to avail this facility, you need to get a particular sticker stuck onto your ID. You can get more information on that once you're here.
* Uber
* Buy your own car. Though I haven't tried this option, I know a lot of international students -- including Indians -- who did. I think GEO explains the process in one of their on-campus meetings. In any case don't forget to bring your Indian driver's licence if you have one.

Bicycles, skateboards, rollerskates, and rollerblades are common ways of getting about in the huge UNM campus.

There are plenty of shopping centers throughout the city. Walgreens -- there's one very close to the UNM campus -- and Walmart Supercenters are obvious places to go check out. There were also some markets selling stuff from all over the world.

![Market](/images/unm-guide/market.jpg)
*I don't remember where this market was -- was accompanying a friend from east Asia. It had stuff from a lot of other countries.*

Have an ID with you at all times, especially when leaving campus. UNM has a campus police, and they're the actual police -- not some security agency. We students get messages on our phones whenever there's criminal activity -- theft, carjacking, assaults etc -- in or near the campus. I'll spare you the details of the gun situation; suffice to say the campus police are at the top of their game.

There are multiple banks in town. Opening a bank account is straightforward -- simply go to a bank and say "I want to open a new account" -- and takes less than 45 minutes. Show your passport as proof of identification. The bank I dealt with gave a temporary debit card immediately; permanent debit card with my name on it was mailed to me within two weeks. On rare occasions, transactions went into a state of limbo if you transact after office hours ― money would be withdrawn from the sender's account, but wouldn't have reached the recipient's account. This got resolved at the start of the next working day. International transactions from India sometimes took a few days to get processed.

Clarify from your bank in your home-country whether the card you have would work in USA; helps if your card is either MasterCard or Visa, but that's no guarantee it'll work. Some banks in India provide what is called a traveller's card or a foreign currency card where you can get foreign currency loaded into this card and use it here. I have tried this facility and it works. 

Albuquerque has its own airport located about 10 minutes away from UNM. The first time your land here, you need to come up with a plan to get from the airport to your location. I'm unsure if Albuquerque Airport has regular taxis anymore. There is the option of connecting to the airport WiFi, installing the American version of Uber app, setting it up with a debit card, and using that to get to your accomodation. I've also heard about volunteers who would pick you up from the airport and drop you off at your accommodation; you'll have to check with GEO to know more.

### Punctuality

Western attitude towards time is different from what I've seen back home. Schedules, appointments, deadlines etc must be strictly adhered to.

Ensure you show up to your classes on time. At the time you register for classes you'll know exactly when and where the classes would meet. If you have registered for back-to-back classes ensure it's physically possible for you to get from the first class' meeting room to the second class' meeting room. If they're in two buildings a mile apart and you only have 5 minutes from the end of the first class' and the beginning of the second, you're not going to make it. Have a general idea of where the buildings are by studying at the [UNM map](https://css.unm.edu/campus-maps/docs/visitormapcentral_alpha.pdf). Or avoid registering for back-to-back classes.

There's this concept of "walk-in hours"; for example if somebody -- such as a professor -- declares their walk-in hours to be from 2 pm to 4 pm on Tuesdays and Thursdays, you can go and see them only at that time. The idea is that they will be available to meet people then, but at other times they may be busy with their actual jobs (such as their research, teaching, or other scheduled tasks).

If you need to see them at any other time, email them and set up an appointment. And respect that appointment! If you can't make it to a meeting after you have scheduled an appointment, email them and let them know in advance.

If meetings have a scheduled end time, make sure the meeting ends at that time. Any of the attendees may have their next thing scheduled right after, so it's inappropriate to go past the end of the meetings.

Businesses too have strict opening and closing times. If a restaurant is supposed to open at 7 am, it won't open a minute sooner even if there's a huge crowd waiting outside. And if it's supposed to at close 10 pm, it will close at 10 pm. So before you go anywhere -- be it the bank, barbershop, library, restaurant, mall, or wherever -- ensure they're open. Their official websites and the map application on your smartphone are good places to start.

Be punctual.

##### Useful Links
* [Chronemics](https://en.wikipedia.org/wiki/Chronemics)

### Visa

I had written more about obtaining a US visa in [another post](/post/getting-a-us-student-visa/ "Getting a US student visa").

The exact visa you need will be mentioned on the documents that you receive from UNM. It'll most likely be something called "F1 visa" (or J1 if you're coming on an exchange program).

Once you get the documents from UNM, apply for your visa; log in to the official U.S. govt. website and make an appointment with your nearest U.S. Consulate in India.

The visa interview is a two-day process:

* Day 1: they take your photograph and fingerprints. 
* Day 2: interview with a US consular officer at the US consulate.

You have to make appointments for both these things, and choosing it to be on two consecutive days is a good idea unless you have plans to hang around the city in the meantime. You can't do both appointments on a single day as far as I know.

Don't believe any horror stories other people tell you about visa interviews. Precisely answer their questions and you'll be fine. The consular officers were very friendly to me, and I had no trouble at all getting a visa.

There's a fee for the visa-application, which you pay online. I strongly recommend applying for the visa directly instead of dealing with any middlemen or agencies.

---

The [Global Education Office (GEO)](https://global.unm.edu/) at UNM handles all things related to international students. Check with them for accurate information and resources.

Once again, let me remind you that it's been nearly 5 years since I left UNM, and the situations may have changed. So, don't take my word for it.

I guess that's just about it. Have a great time at UNM! Good luck!

[(Back to table of contents.)](#table-of-contents)
