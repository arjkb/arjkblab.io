---
layout: post
title: Fedora 24 Impressions
date: 2016-07-16
excerpt: <p> I've been using Fedora 24 for about three weeks now. Here's my take </p>
tags: fedora, linux
---
Though it hasn't been long since I've switched to Fedora as my everyday OS, I've become a huge fan of it.

Ubuntu was consistently giving me problems (like, freezes for instance), and that's when I had decided to make the switch. (However, I reckon the problems I've been facing with Ubuntu are more of a Unity problem than an Ubuntu problem).

Never had any deal-breaking issues with Fedora.

And then, there are some nice touches like the small box that pops up when you create or rename a folder. Polished, though not quite in the leagues of the Macs.

Amidst some delays, Fedora has finally come up with their newer version, the Fedora 24, and I have almost immediately switched over to its workstation edition.

![Fedora 24 Details page](/images/f24_details.png)

Installation was mostly straightforward, although the first time I "tried" Fedora 24, it wasn't detecting the spacebar presses. [Apparently, I wasn't the only one facing issues with the spacebar.](http://forums.fedoraforum.org/showthread.php?t=310369) However, it just occurred only that one time, and has never recurred.

[Fedora magazine][1] has documented the [changes in Fedora 24][2] much more conscisely, and so I'm not going to focus on all of that in this post. What I do intend to point out are the noticeable changes from an everyday programmer's perspective.

The most evident change that strikes you is that the [fonts have become much better.][3] I've mentioned before that the single biggest thing I hate about the vanilla installation of previous versions of Fedora is its fonts, particularly its aliasing. _Used to look very 90s._

As a matter of fact, the first thing I used do right after installing Fedora was to manually tweak the aliasing of the fonts to make it match with that of Ubuntu (where fonts look substantially better).

However, in this newer Fedora, the issue seems to have been resolved. The fonts look much better out of the box, and I did not have to do any manual tweaks.

And then, a friend of mine who bought a newer model ASUS laptop had trouble with Linux not detecting his touchpad, and an unreliable WiFi connectivity (he was forced to use a wired connection, thus making his laptop practically a desktop). However, with Fedora 24 his laptop's issues have been resolved. _We never bothered to try Ubuntu._

Though not strictly a Fedora feature, another welcome change I have noticed is the updated version of gcc. Fedora now comes with gcc version 6.1.1, and it provides substantially more useful error messages that helps you to pinpoint your error quicker. For illustration, I've cooked up a small program where I _forgot_ to include the math.h header file, and here's what gcc comes up with:

<!-- Image here  -->
![gcc newer screenshot](/images/gcc_6_1_1.png)

This is not the first time a compiler has been providing me with useful error messages. The [clang](http://clang.llvm.org/) compiler front-end backed by the big names in the industry also had similar functionalities, but then I preferred gcc because it tends to compile projects -- especially the non-trivial ones -- noticeably faster. _(However, I'm aware that this could change in the future)._

Overall, I'm very satisfied with the new Fedora 24, and if you're planning to change your existing distro, please do [try out Fedora 24.][4]

[1]: https://fedoramagazine.org/
[2]: https://fedoramagazine.org/whats-new-fedora-24-workstation/
[3]: https://fedoramagazine.org/font-improvements-fedora-24-workstation/
[4]: https://getfedora.org/
